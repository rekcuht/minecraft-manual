module.exports = {
    title: 'Minecraft Manual',
    base: '/minecraft-manual/',
    themeConfig: {
      sidebar: 'auto',
      nav: [
        { text: 'Top', link: '/' },
        { text: 'MOD', link: '/mod/' }
      ]
    }
  }
