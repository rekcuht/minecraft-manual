# 影MOD導入手順

## 影MODとは
マイクラの見た目のクオリティを上げるMOD  
こんくらい違う

<img src="shader/before.png" style="width: 45%">
<img src="shader/after.png" style="width: 45%">


影MODは`シェーダーパック`と、それを適用する`MOD(OptiFine)`からなる。  
ここでは比較的軽いシェーダーパック(`KUDA shaders`)の入れ方を載せる。  
上の画像も`KUDA shaders`で撮ったやつ


## 前提条件
マイクラの`1.13.2`で一度起動していること


## 大まかな導入手順
1. `OptiFine`とかいうMODを入れる
1. `KUDA shaders`を`shaderpacks`フォルダに入れる
1. ワールドに入って、`ゲーム内の設定`でKUDA shadersを適用する

詳細は以下


## `Optifine`の導入
シェーダーパックを適用するMOD、`OptiFine`を導入する

### ダウンロード
[ここ](shader/OptiFine_1.13.2_HD_U_E7.jar)からダウンロード

::: tip 詳細 (規約違反防止で念のため記載)(無視しておｋ)
公式: [Optifine HD Mod for Minecraft](http://shadersmods.com/optifine-hd-mod/)  
バージョン: 1.13.2_HD_U_E7
:::

### インストール
#### ダウンロードした`OptiFine_1.13.2_HD_U_E7.jar`を右クリして`Java`から実行する。  
![](open-jar.png)  
これが出ない人は`Java`が入ってない可能性がある。  
「`Java インストール`」とかで調べるか直接誰かに聞いてくれ

#### `Install`を押してインストール開始
![](shader/install-optifine.png)

#### ↓が表示されたら`OK`押して完了
![](shader/install-optifine-finish.png)


## `KUDA shaders`の導入
### ダウンロード
規約上、公式からダウンロードする必要がある。  
というわけで以下がダウンロード手順

1. [Minecraft KUDA-Shaders – DeDelner](https://dedelner.net/kuda-shaders/)にアクセス
1. `KUDA-Shaders v6.1 Legacy`をクリック (広告がうっさいかも)  
![](shader/download-kuda.png)
1. 開いたページの右上が`SKIP AD`になったら押す  
![](shader/download-kuda-2.png)
1. `Download`を押す  
![](shader/download-kuda-3.png)
1. `KUDA-Shaders v6.1 Legacy.zip`がダウンロードされる

### `shaderpacks`に入れる
#### `Windowsキー`と`R`を同時押しして、`%appdata%\.minecraft`と入力してエンター
![](shader/open-minecraft-dir.png)

#### `shaderpacks`フォルダに、ダウンロードした`KUDA-Shaders v6.1 Legacy.zip`を入れる
**(※ `OptiFine`を入れてから一度起動しないと`shaderpacks`フォルダはできない)**
![](shader/shaderpacks.png)  
↓  
![](shader/shaderpacks-2.png)

これで準備完了


### ゲーム内で設定する
![](shader/shader-setting.png)  
![](shader/shader-setting-2.png)  
![](shader/shader-setting-3.png)


# おわり！！！！！！！！！！

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

なんか物足りないって？  
そんな君は`SEUS`シェーダーを使ってみよう  
`KUDA shaders`と同じように`shaderpacks`フォルダに入れるだけだ

## ガチ影MOD: `SEUS`編
### SEUSのダウンロード
1. [Sonic Ether's Unbelievable Shaders](https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-mods/1280299-sonic-ethers-unbelievable-shaders-compatible-with)を開く

1. `SEUS Renewed 1.0.0 (DOWNLOAD)`をクリック  
![](shader/download-seus.png)

1. `Download`をクリック  
![](shader/download-seus-2.png)

1. `Agree`にチェック入れて`Download`をクリック  
![](shader/download-seus-3.png)

1. `SEUS-Renewed-1.0.0.zip`がダウンロードされる


### `shaderpacks`に入れる
ダウンロードした`SEUS-Renewed-1.0.0.zip`を`shaderpacks`ディレクトリに入れる。  
手順は`KUDA shaders`の時と同じ。`Windowsキー`と`R`を同時押しして、`%appdata%\.minecraft`  
![](shader/put-seus.png)

### ゲーム内で設定する
![](shader/shader-setting-seus.png)


# owari
重かったらKUDAに切り替えて
